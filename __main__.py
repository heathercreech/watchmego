#!/usr/bin/python3
import os
import subprocess
import time
from fnmatch import fnmatch

import click
import pyinotify

GOPATH = os.environ['GOPATH']
GOBIN = os.path.join(GOPATH, 'bin')

def build_go(directory):
    click.echo('Building ' + directory + '...');
    completed = subprocess.run('go install ' + directory, stdout=subprocess.PIPE, shell=True)
    retcode = completed.returncode

    if retcode:
        click.echo('Install failed')
        return False
    else:
        click.echo('Install successful')
        return True

def launch_app(exe_name):
    subprocess.Popen(os.path.join(GOBIN, exe_name))
    click.echo('Application ready!')

def relaunch_app(exe_name):
    click.echo('Relaunching application...')
    subprocess.run(['pkill', exe_name])
    launch_app(exe_name)

class GoHandler(pyinotify.ProcessEvent):
    patterns = ['*.go']

    def __init__(self, directory):
        super().__init__()
        self.directory = directory
        self.exe_name = directory.split('/')[1]

    #Check if file matches glob pattern
    def process(self, event):
        for pattern in self.patterns:
            if fnmatch(event.name, pattern):
                if build_go(self.directory):
                    relaunch_app(self.exe_name)
                break;

    def process_IN_CLOSE_WRITE(self, event):
        self.process(event)

@click.command()
@click.argument('directory')
def watch_init(directory):
    click.echo('Initializing...')
    watch_path = os.path.join(os.environ['GOPATH'], 'src', directory)
    watch_manager = pyinotify.WatchManager()
    watch_manager.add_watch(watch_path, pyinotify.ALL_EVENTS, rec=True)

    event_handler = GoHandler(directory)
    notifier = pyinotify.Notifier(watch_manager, event_handler)
    click.echo('Watching directory: ' +  directory)
    launch_app(event_handler.exe_name)
    notifier.loop()

if __name__ == '__main__':
    watch_init()
